﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Application.DataContext;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.Navigation.ContextNavigation;
using JetBrains.ReSharper.Feature.Services.Navigation.NavigationExtensions;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.Caches;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using JetBrains.ReSharper.Resources.Shell;

namespace HrorCommandsPlugin
{
    [ContextNavigationProvider]
    public class NavigateToHandler : INavigateFromHereProvider
    {
        private readonly string[] Interfaces = new[] { "IMessage", "ICommand", "IEvent" };

        public IEnumerable<ContextNavigation> CreateWorkflow(IDataContext dataContext)
        {
            var node = dataContext.GetSelectedTreeNode<ITreeNode>();
            var typeDeclaration = node as IClassDeclaration;

            if (typeDeclaration == null)
            {
                typeDeclaration = node?.GetParentOfType<IClassDeclaration>();
            }

            var interfaces = typeDeclaration?.ExtendsList?.ExtendedInterfaces;

            if (interfaces != null && interfaces.Value.Any(x => Interfaces.Any(i => x.GetText() == i)))
            {
                ISolution solution = Shell.Instance.GetComponent<SolutionsManager>().Solution;

                var usages = SearchHandlers(typeDeclaration, solution);
                var i = 0;
                foreach (var declaredTypeUsage in usages)
                {
                    i++;
                    yield return new ContextNavigation($"Handler {i}", null, NavigationActionGroup.Other, () =>
                    {
                        declaredTypeUsage.NavigateToTreeNode(true);
                    });
                }
            }
        }

        public IEnumerable<ITreeNode> SearchHandlers(IClassDeclaration typeDeclaration, ISolution solution)
        {
            var messageName = typeDeclaration.NameIdentifier.GetText();
            var symbolCache = solution.GetComponent<ISymbolCache>();

            var classList = symbolCache
                            .Parts
                            .Load()
                            .Where(o => o.Key.Name.Contains("Handl")) // hack for better speed
                            .Select(o => o.Key)
                            .SelectMany(symbolCache.GetTypesAndNamespacesInFile)
                            .OfType<JetBrains.ReSharper.Psi.ExtensionsAPI.Caches2.Class>()
                            .Where(x => x.Methods.Any(m => m.ShortName == "Handle"
                                                          && m.Parameters
                                                          .Any(p => p.Type.GetPresentableName(x.PresentationLanguage) == messageName)))
                            .Select(x => x.GetFirstDeclaration<IClassDeclaration>())
                            .Where(x => x != null && x.ExtendsList != null && x.ExtendsList.ExtendedInterfaces.Count > 0)
                            .Distinct()
                            .ToList();

            return (from typeElement in classList
                    from implInterface in typeElement.ExtendsList.ExtendedInterfaces
                    where implInterface.GetText().Contains("Handler")
                          && implInterface.GetText().Contains(messageName)
                    select typeElement.Body.Methods.First(x => x.GetText().Contains(messageName)) as ITreeNode)
                    .Distinct(x => x.GetText()).ToList();
        }
    }
}
